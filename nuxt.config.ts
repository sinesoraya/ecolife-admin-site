// import { defineNuxtConfig } from 'nuxt'
// import eslintPlugin from 'vite-plugin-eslint'

// https://v3.nuxtjs.org/api/configuration/nuxt.config
export default defineNuxtConfig({
    app: {
        head: {
            viewport: 'width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no',
        },
    },
    // buildModules: [ '@pinia/nuxt' ],
    css: [
        // '@/styles/antd/index.less',

    ],
    ssr: false,
    runtimeConfig: {
        // public: process.env,
    },

    vite: {
        plugins: [
            // eslintPlugin(),
            // vue({
            //     template: {
            //         compilerOptions: {
            //             isCustomElement: (tag) => ['a-'].includes(tag)
            //         }
            //     }
            // }),
        ],
        css: {
            preprocessorOptions: {
                less: {
                    javascriptEnabled: true,
                },
                scss: {
                    additionalData: '@import "@/styles/fonts/index.scss";',
                },
            },
        },

    },
})
